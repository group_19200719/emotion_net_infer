# Warning!

Этот репозиторий устарел, - сейчас используется ([ссылка](https://gitlab.com/group_19200719/multimodal-emotion-recognition_infer)).

# Описание

Python-package содержит необходимые классы и функции для инференса модели для классификации эмоций человека.

# Пример использования в вашем проекте

Ниже показан базовый пример использования на `python`.

```python
import os
from pathlib import Path

from dotenv import find_dotenv, load_dotenv
from emotion_net_infer import EmotionNet


def read_env_variables():
    load_dotenv(find_dotenv())


def chdir_to_projects_root():
    project_dir = Path(find_dotenv()).parent
    os.chdir(project_dir)


if __name__ == "__main__":
    # сменим директорию на корневой каталог проекта
    chdir_to_projects_root()

    # считаем переменные в рабочее окружение
    read_env_variables()
    
    # сохраним значение из переменной окружения MLFLOW_SERVER_URI в
    # переменную
    mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")

    # инициализируем объект для инференса нейросети
    emotion_net = EmotionNet(mlflow_server_uri)

    # укажем путь к тестовой картинке
    path_to_image = "repo_pics/example_for_emotion_recognition.png"

    # осуществим предсказания на указанном изображении
    emotion_cls, score = emotion_net.load_image_and_predict(path_to_image, True)

    # вывод результатов
    print(emotion_cls, score)

    # результат предсказания:
    # neutral 0.4857593774795532
```

Объект класса `EmotionNet` берет последнюю `Production` версию нейросети из `MlFlow Registry` из docker-контейнера с `MLFlow` к которому подключен.

![MlFlow_Registry](repo_pics/MlFlow_Registry.jpg)

При инициализации объекта `EmotionNet` ему необходимо передать такие параметры как:

* `mlflow_server_uri` : str
    адрес сервера и порт с `MLFlow`, например http://100.100.100.100:9000
* `model_stage` : str
    название стадии разработки модели, которую мы хотим забрать из MLFlow.
    По умолчанию, берется последняя модель версии `Production`.

Данные для подключения к `MLFlow`-серверу по соображениям безопасности хранятся внутри `.env`-файла.

Данный файл должен находиться в корневом каталоге проекта который вы разрабатываете.

Пример, содержимого `.env`-файла представлено ниже:

```
AWS_ACCESS_KEY_ID=your_s3_storage_login
AWS_SECRET_ACCESS_KEY=your_s3_storage_password

MLFLOW_SERVER_URI=http://100.100.100.100:5000
MLFLOW_S3_ENDPOINT_URL=http://100.100.100.100:9000
```

При запуске примера вызов функции `chdir_to_projects_root()` сменит рабочую директорию на кореневой каталог вашего проекта.

Вызов функции `read_env_variables()` почитает из `.env`-файла переменные окружения, необходимые для подключения к серверу `MLFlow Registry` и его `s3-хранилищу` с обученными моделями.

В переменную `mlflow_server_uri` будет записано значение переменной `MLFLOW_SERVER_URI` которое мы и передадим создаваемому объекту класса `EmotionNet`:
```python
emotion_net = EmotionNet(mlflow_server_uri)
```

Осуществим предсказание на выбранном изображении
```
emotion_cls, score = emotion_net.load_image_and_predict(path_to_image, True)
```

Выводим на печать результат предсказания:
```python
print(emotion_cls, score)

# результат предсказания:
# neutral 0.4857593774795532
```

# Документация API для класса EmotionNet

Полный API для класса `EmotionNet` приведен в документации ([ссылка](https://glcdn.githack.com/group_19200719/emotion_net_infer/-/raw/master/html/EmotionNetInference.html)).

Данная документация была сгенерирована с помощью `pdoc3`([ссылка](https://pdoc3.github.io/pdoc/)).

`pdoc3` включен в список зависмостей для `dev`-группы `poetry`.
Чтобы сгенерировать документацию для `EmotionNetInference` выполним команду:
```
pdoc --html emotion_net_infer/EmotionNetInference.py
```

Исходный html-код появится в папке `html/EmotionNetInference.html`.

На его основе сервис `raw.githack.com`([ссылка](https://raw.githack.com/)) создает статический веб-сайт.